// Testing file
const chai = require('chai');
const chaiHttp = require('chai-http');

// App instance
const app = require('../app');

// Database connection
chai.use(chaiHttp);
const expect = chai.expect;

// Testing data
const clientToAdd = {
    nom : 'test',
    prenom : 'test',
    date : Date.now,
    telephone : 'test',
    avance : 0,
    rest : 0
};



// Doing the tests
describe('"clients" side only', () => {
    it('get the list of clients', (done) => {
        chai.request(app)
            .get("/api/clients")
            .end((err, res) => {
                expect(res).to.have.status(200);
                done();
              });
    });

    it('create a new client', (done) => {
        chai.request(app)
            .post("/api/clients")
            .send(clientToAdd)
            .end( async (err, res) => {
                expect(res).to.have.status(200);

                const createdClient = await mongoose.model('Client').findOne({ nom: 'test' });
                expect(createdClient).to.exist;
                expect(createdClient.prenom).to.equal('test');
                done()
            })
    });
});



