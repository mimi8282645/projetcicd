//#region Importement des modules
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var multer = require('multer');
require('dotenv').config();
//#endregion




// App instance
var app = express();
const port = 5000;


// Database connection
const dbURI = process.env.MDB_URI;
mongoose.connect(dbURI, { useNewUrlParser: true, useUnifiedTopology: true })
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));


//#region Database Schema
const clientSchema = new mongoose.Schema({
  nom : {type : String, required : true},
  prenom : {type : String, required : true},
  date : {type : Date, required : true, default : Date.now},
  telephone : {type : String, required : true},
  avance : {type : Number, required : false},
  rest : {type : Number, required : false},
});

const facturesSchema =  new mongoose.Schema({
  
  // practical informations
  ownerId : mongoose.Schema.ObjectId,
  date : {type : Date, default : Date.now},

  // file informations
  filename : String,
  content : Buffer,

})
//#endregion


// database models
const Client = mongoose.model('Client', clientSchema);
const Factures = mongoose.model('Factures', facturesSchema);



// multer storage
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/'); 
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-' + file.originalname); 
  }
});

const filter = (req, file, cb) => {
  if (file.mimetype === "application/pdf") {
    cb(null, true);
  } else {
    cb(new Error("Only PDF files are allowed"), false);
  }
};

const upload = multer({ storage: storage, fileFilter: filter });


//#region api endpoints

//#region clients
app.get('/api/clients', async (req, res) => {
  const clients = await Client.find();
  res.send(clients);
});

app.post('/api/clients', async (req, res) => {
  const client = new Client({
    nom : req.body.nom,
    prenom : req.body.prenom,
    telephone : req.body.telephone,
    avance : req.body.avance,
    rest : req.body.rest
  });
  await client.save();
  res.send(client);
});


app.put('/api/clients/:id', async (req, res) => {
  const client = await Client.findByIdAndUpdate(req.params.id, {
    nom : req.body.nom,
    prenom : req.body.prenom,
    telephone : req.body.telephone,
    avance : req.body.avance,
    rest : req.body.rest
  }, {new : true});
  res.send(client);
});

app.delete('/api/clients/:id', async (req, res) => {
  const client = await Client.findByIdAndRemove(req.params.id);
  res.send(client);
});
//#endregion

//#region factures
app.get('/api/clients/:id/factures', async (req, res) => {
  const factures = await Factures.find({ownerId : req.params.id});
  res.send(factures);
});


app.post('/api/clients/:id/factures', upload.single('facture'), async (req, res) => {
  const client = await Client.findById(req.params.id);
  if(!client) return res.status(404).send('Client not found');
  const facture = new Factures({
    ownerId : client._id,
    date : Date.now(),
    content : req.file.buffer,
    filename : req.file.originalname
  });
  await facture.save();
  res.send(facture);
});


app.put('/api/clients/:id/factures/:factureId', upload.single('facture'), async (req, res) => {
  const client = await Client.findById(req.params.id);
  if(!client) return res.status(404).send('Client not found');
  const facture = await Factures.findByIdAndUpdate(req.params.factureId, {
    ownerId : client._id,
    date : Date.now(),
    content : req.file.buffer,
    filename : req.file.originalname
  }, {new : true});
  res.send(facture);
});


app.delete('/api/clients/:id/factures/:factureId', async (req, res) => {
  const client = await Client.findById(req.params.id);
  if(!client) return res.status(404).send('Client not found');
  const facture = await Factures.findByIdAndRemove(req.params.factureId);
  res.send(facture);
});
//#endregion

//#endregion

//#region view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
//#endregion


//#region error handling
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

//#endregion

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
});
module.exports = app;
